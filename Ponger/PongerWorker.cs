﻿using Ponger.Interfaces;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Services;
using System.Threading.Tasks;

namespace Ponger
{
    public class PongerWorker : IPongerWorker
    {
        private readonly MessageQueueService _messageQueueService;

        public PongerWorker()
        {
            _messageQueueService = new MessageQueueService();
        }

        public void ListenQueue()
        {
            _messageQueueService.ListenQueue(
                "PongerExchange",
                "pong_queue",
                "pong_key",
                ReceivedAsync);
        }

        public void SendMessageToQueue()
        {
            _messageQueueService.WriteToQueue(
                "PingerExchange",
                "ping_queue",
                "ping_key");
        }

        private async void ReceivedAsync(object sender, BasicDeliverEventArgs args)
        {
            ListenQueue();
            await Task.Delay(2500);
            _messageQueueService.SendMessageToQueue("pong");
        }

        public void Dispose()
        {
            _messageQueueService.Dispose();
        }
    }
}
