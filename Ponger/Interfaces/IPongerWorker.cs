﻿using System;

namespace Ponger.Interfaces
{
    interface IPongerWorker : IDisposable
    {
        void ListenQueue();
        void SendMessageToQueue();
    }
}
