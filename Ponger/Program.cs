﻿using System;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var pongWorker = new PongerWorker();
            Console.WriteLine("Ponger started! (click enter to exit)");
            pongWorker.ListenQueue();
            pongWorker.SendMessageToQueue();
            while (Console.ReadKey().Key != ConsoleKey.Enter) ;
        }
    }
}
