﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Globalization;
using System.Text;

namespace RabbitMQ.Wrapper.Services
{
    public class MessageQueueService : IMessageQueue
    {
        private IMessageConsumerScope _messageConsumerScope;
        private IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScopeFactory _messageConsumerScopeFactory;
        private readonly IMessageProducerScopeFactory _messageProducerScopeFactory;

        public MessageQueueService()
        {
            var connectionFactory = new ConnectionFactory(new Uri(Resource.Rabbit));

            _messageConsumerScopeFactory = new MessageConsumerScopeFactory(connectionFactory);
            _messageProducerScopeFactory = new MessageProducerScopeFactory(connectionFactory);
        }

        public void ListenQueue(string exchangeName, string queueName, string routingKey, 
            EventHandler<BasicDeliverEventArgs> SuccessfullyReceived)
        {
            _messageConsumerScope = _messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = exchangeName,
                ExchangeType = ExchangeType.Direct,
                QueueName = queueName,
                RoutingKey = routingKey
            });

            _messageConsumerScope.MessageConsumer.Received += MessageRecieved;
            _messageConsumerScope.MessageConsumer.Received += SuccessfullyReceived;
        }

        public void WriteToQueue(string exchangeName, string queueName, string routingKey)
        {
            _messageProducerScope = _messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = exchangeName,
                ExchangeType = ExchangeType.Direct,
                QueueName = queueName,
                RoutingKey = routingKey
            });
        }

        public bool SendMessageToQueue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                Console.WriteLine($"[x] Send {value} at " +
                    $"{DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"))}");
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        private void MessageRecieved(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                Console.WriteLine($"[x] Recieved {value} at " +
                    $"{DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"))}");
                processed = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                processed = false;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        public void Dispose()
        {
            _messageConsumerScope.Dispose();
            _messageProducerScope.Dispose();
        }
    }
}
