﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScopeFactory
    { 
        IMessageProducerScope Open(MessageScopeSettings messageScopeSettings);
    }
}
