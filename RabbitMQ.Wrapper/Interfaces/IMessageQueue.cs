﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    interface IMessageQueue : IDisposable
    {
        void ListenQueue(string exchangeName, string queueName, string routingKey,
            EventHandler<BasicDeliverEventArgs> SuccessfullyReceived);
        void WriteToQueue(string exchangeName, string queueName, string routingKey);
        bool SendMessageToQueue(string value);
    }
}
