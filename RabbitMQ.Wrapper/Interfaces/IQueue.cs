﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IQueue : IDisposable
    {
        IModel Channel { get; }
        void DeclareExchange(string exchangeName, string exchangeType);
        void BindQueue(string exchangeName, string routingKey, string queueName);
    }
}
