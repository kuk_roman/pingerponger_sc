﻿using System;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            using var pingWorker = new PingerWorker();
            Console.WriteLine("Pinger started! (click enter to exit)");
            pingWorker.ListenQueue();
            pingWorker.SendMessageToQueue();
            pingWorker.Start();
            while (Console.ReadKey().Key != ConsoleKey.Enter) ;
        }
    }
}
