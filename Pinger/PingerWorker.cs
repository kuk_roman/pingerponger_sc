﻿using Pinger.Interfaces;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Services;
using System;
using System.Threading.Tasks;

namespace Pinger
{
    public class PingerWorker : IPingerWorker
    {
        private readonly MessageQueueService _messageQueueService;

        public PingerWorker()
        {
            _messageQueueService = new MessageQueueService();
        }

        public void Start()
        {
            _messageQueueService.SendMessageToQueue("ping");
        }

        public void ListenQueue()
        {
            _messageQueueService.ListenQueue(
                "PingerExchange",
                "ping_queue",
                "ping_key",
                SuccessfullyReceivedAsync);
        }

        public void SendMessageToQueue()
        {
            _messageQueueService.WriteToQueue(
                "PongerExchange",
                "pong_queue",
                "pong_key");
        }

        async void SuccessfullyReceivedAsync(object sender, BasicDeliverEventArgs args)
        {
            ListenQueue();
            await Task.Delay(2500);
            _messageQueueService.SendMessageToQueue("ping");
        }

        public void Dispose()
        {
            _messageQueueService.Dispose();
        }
    }
}
