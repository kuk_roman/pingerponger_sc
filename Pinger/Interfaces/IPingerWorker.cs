﻿
using System;

namespace Pinger.Interfaces
{
    interface IPingerWorker : IDisposable
    {
        void ListenQueue();
        void SendMessageToQueue();
    }
}
